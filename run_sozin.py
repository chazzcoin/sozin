from rsvaatu.SozinMaster.RssFeedClient import RssFeedClient
from rsvaatu.SozinMaster.RedditClient import RedditClient
from rsvaatu.SozinMaster.TwitterClient import TwitterClient
from rsvaatu.SozinMaster.Analyzers import SozinAnalyzer, Summarizer

overall_crypto_tickers = []
overall_stock_tickers = []

twitter = []
reddit = []
articles = []


def run_twitter_analysis():
    tw = TwitterClient.TwitterClient()
    tw.get_all_tweets()
    pr_twitter = SozinAnalyzer.ProcessWords(tweets=tw.tweets, reddit_comments=None, articles=None)
    print("\n -> Twitter <- \n")
    analyzer = SozinAnalyzer.SozinAnalyzer(cryto_tickers=pr_twitter.cryto_tickers,
                                           stock_tickers=pr_twitter.stock_tickers,
                                           comments=pr_twitter.comments)
    return analyzer
    # analyzer.print_analysis()
    # print("\nOriginal Tweeet")
    # print(pr_twitter.comments['BTC'][0])
    # print("\nNew Tweet")
    # Summarizer.generate_summary(pr_twitter.comments['BTC'][0])
    # results = Analyzers.get_results()
    # print("FINAL CRYPTO: \n", results.get("crypto_tickers"))
    # print("FINAL STOCKs: \n", results.get("stock_tickers"))


def run_reddit_analysis():
    rd = RedditClient.RedditClient(isCrypto=True, isStocks=True)
    pr_reddit = SozinAnalyzer.ProcessWords(tweets=None, reddit_comments=rd.comments, articles=None)
    print("\n -> Reddit <- \n")
    obj = SozinAnalyzer.SozinAnalyzer(cryto_tickers=pr_reddit.cryto_tickers,
                                      stock_tickers=pr_reddit.stock_tickers,
                                      comments=pr_reddit.comments)
    obj.print_analysis()
    if len(pr_reddit.comments) > 0:
        Summarizer.generate_summary(pr_reddit.comments[0])


def run_article_analysis():
    rss = RssFeedClient.rssDownloader()
    pr_articles = SozinAnalyzer.ProcessWords(tweets=None, reddit_comments=None, articles=rss.list_of_articles)
    print("\n -> Articles <- \n")
    SozinAnalyzer.SozinAnalyzer(cryto_tickers=pr_articles.cryto_tickers,
                                stock_tickers=pr_articles.stock_tickers,
                                comments=pr_articles.comments).print_analysis()


def summarize_article():
    url2 = "https://www.thestreet.com/investing/cannabis/cannabis-stocks-watchlist-cresco-labs-village-farms-trulieve-curaleaf-tilray"
    url = "https://finance.yahoo.com/news/tilray-set-another-short-squeeze-235541898.html"
    url3 = "https://www.marketwatch.com/story/cannabis-stocks-see-more-broad-gains-as-sector-tracker-heads-for-best-week-since-the-election-2021-02-05"
    url4 = "https://www.nbcnews.com/politics/trump-impeachment-inquiry/trump-s-second-impeachment-trial-kick-tuesday-n1257119"
    url5 = "https://www.globenewswire.com/news-release/2021/02/12/2174847/0/en/MoneyLion-America-s-Leading-Digital-Financial-Platform-to-Become-Publicly-Traded-via-Merger-with-Fusion-Acquisition-Corp-NYSE-FUSE.html"
    rss = RssFeedClient.rssDownloader.get_single_article(url5)
    # SozinMaster.sozin_utils.summarizer.generate_summary(rss.text)
    ar = SozinAnalyzer.ProcessWords.sort_by_master_terms(rss.text)
    for a in ar:
        print(a + ":", ar[a])
    # print("\n SozinMaster Analyzer \n")
    # pr_article = sozin_analyzer.ProcessWords(tweets=None, reddit_comments=None, articles=None, article=rss)
    # Analyzers = sozin_analyzer.SozinAnalyzer(cryto_tickers=pr_article.cryto_tickers,
    #                                         stock_tickers=pr_article.stock_tickers,
    #                                         comments=pr_article.comments)
    # Analyzers.print_analysis()
    # report = Analyzers.get_results()
    # print("FINAL CRYPTO: \n", report.get("crypto_tickers"))
    # print("FINAL STOCKs: \n", report.get("stock_tickers"))

# def run_FULL_analysis():
#     print("\n -> All Sources <- \n")
#     pr_all = sozin_analyzer.ProcessWords(tweets=tw.tweets, reddit_comments=rd.comments, articles=rss.list_of_articles)
#     sozin_analyzer.SozinAnalyzer(pr_all.cryto_tickers, pr_all.stock_tickers, pr_all.comments).print_analysis()


# run_twitter_analysis()
# run_reddit_analysis()
# run_article_analysis()
# summarize_article()


