import feedparser
from newspaper import Article
import rsvaatu.SozinMaster.sozin_utils.sozin_sources


class rssDownloader:
    url = ""
    list_of_urls = []
    list_of_articles = []
    debug = False
    downloaded = 0
    failed_to_download = 0

    def __init__(self, debug=False, url=None, rss_urls=rsvaatu.SozinMaster.sozin_utils.sozin_sources.rss_url_list):
        self.debug = debug
        if rss_urls is not None:
            self.get_links_from_rss_list(rss_urls)
        elif url is not None:
            self.url = url
            self.get_links_from_rss(url)

    def get_links_from_rss(self, rss_url):
        feed = feedparser.parse(rss_url)
        for entry in feed.entries:
            article_link = entry.link
            self.download_article(article_link)

    def get_links_from_rss_list(self, rss_urls: []):
        print("Downloading Articles...")
        for url in rss_urls:
            feed = feedparser.parse(url)
            for entry in feed.entries:
                article_link = entry.link
                self.list_of_urls.append(article_link)
        for ar_url in self.list_of_urls:
            self.download_article(ar_url)
        print("\nSuccessfully downloaded " + str(self.downloaded) + " articles.\n")
        print("\nFailed to download " + str(self.failed_to_download) + " articles.\n")

    def download_article(self, url):
        article = Article(url)
        try:
            article.download()
            article.parse()
            self.list_of_articles.append(article)
            self.downloaded += 1
            if self.debug:
                print("Successfully Downloaded Article.")
        except:
            self.failed_to_download += 1
            if self.debug:
                print("Failed to Download Article.")

    @staticmethod
    def get_single_article(url):
        art = Article(url)
        try:
            art.download()
            art.parse()
            print("Successfully Downloaded Article.")
            return art
        except:
            print("Failed to Download Article.")

