import tweepy
from tweepy import OAuthHandler

from rsvaatu.SozinMaster import sozin_utils
from rsvaatu.SozinMaster.sozin_utils import sozin_config, sozin_sources


class TwitterClient:

    tweets = []
    debug = False
    # keys and tokens from the Twitter Dev Console
    consumer_key = sozin_config.twitter_consumer_key
    consumer_secret = sozin_utils.sozin_config.twitter_consumer_secret
    access_token = sozin_utils.sozin_config.twitter_access_token
    access_token_secret = sozin_utils.sozin_config.twitter_access_token_secret
    number_of_tweets = sozin_utils.sozin_config.number_of_tweets
    api = None

    def __init__(self, debug=True):
        self.debug = debug
        # attempt authentication
        try:
            # create OAuthHandler object
            auth = OAuthHandler(self.consumer_key, self.consumer_secret)
            # set access token and secret
            auth.set_access_token(self.access_token, self.access_token_secret)
            # create tweepy API object to fetch tweets
            self.api = tweepy.API(auth)
        except:
            print("Error: Authentication Failed")

    def get_all_tweets(self):
        # LOOP through list of Users to Monitor
        for user in sozin_sources.twitter_users:
            self.get_tweets_for_user(user)

    def get_tweets_for_user(self, user):
        tweets_api = self.api.user_timeline(
            # User to Grab From
            screen_name=user,
            # Tweets Limit
            count=self.number_of_tweets,
            # Necessary to keep full_text
            include_rts=False,
            # otherwise only the first 140 words are extracted
            tweet_mode='extended')

        # If DEBUG MODE, print every tweet
        if self.debug:
            for t in tweets_api:
                self.tweets.append(t.full_text)
                print("ID: {}".format(user))
                print(t.created_at)
                print(t.full_text)
                print("\n")