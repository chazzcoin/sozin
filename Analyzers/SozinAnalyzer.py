from nltk.sentiment.vader import SentimentIntensityAnalyzer
import pandas as pd

import regex as re

from rsvaatu.SozinMaster.sozin_utils import crypto_data, stock_data, sozin_config, sozin_sources


def remove_special_characters(text):
    newText = re.sub('[^a-zA-Z]', '', text)
    return newText


class ProcessWords:
    stock_tickers = {}
    cryto_tickers = {}
    comments = {}
    categories = {}
    count = 0

    def __init__(self, tweets=None, reddit_comments=None, articles=None, article=None):
        if tweets is not None:
            for words in tweets:
                self.sort_words(words)
        if reddit_comments is not None:
            for words in reddit_comments:
                self.sort_words(words)
        if articles is not None:
            for art in articles:
                self.sort_words(art.text)
        if article is not None:
            self.sort_words(article.text)

    def sort_words(self, arText):
        split = arText.split(" ")
        for word in split:
            newWord = ProcessWords.pre_process_word(word)
            self.process_word(newWord, arText)

    @staticmethod
    def sort_by_category(arText):
        master = {}
        for masterItem in sozin_sources.topics.keys():
            cats = {}
            for item in sozin_sources.topics[masterItem]:
                if item in arText:
                    if item in cats:
                        cats[item] += 1
                    else:
                        cats[item] = 1
            if masterItem not in master:
                master[masterItem] = cats
        return master

    @staticmethod
    def sort_by_master_terms(arText):
        master_words = {}
        for item in sozin_sources.stock_terms:
            word = {}
            if item in arText:
                if item in master_words:
                    word[item] += 1
                else:
                    word[item] = 1

            if item not in master_words:
                master_words[item] = word
        return master_words

    @staticmethod
    def pre_process_word(preWord):
        return preWord.replace(",", "") \
            .replace("\'s", "") \
            .replace("/USD", "")\
            .replace(")", "")\
            .replace("(", "")\
            .replace("#", "")\
            .replace(".", "")\
            .replace("!", "")\
            .replace("?", "")

    """ -> Algo to Process if (word) is a Crypto or a Stock <- """
    def process_word(self, word, arText):
        # test = word
        # upper = word.upper()
        if word.startswith("@"):
            return
        w = word.upper()
        # -> 1. is word name of network, not ticker name
        if len(w) >= 5 and w in crypto_data.CRYPTO_NAMES.keys():
            word = crypto_data.CRYPTO_NAMES[w]
            self.add_crypto_ticker(word, arText)
            return
        # -> 2. If word is longer than 5 character, can't be a ticker.
        if len(word) > 5:
            return
        # -> 3. is (word) a Stock
        if word.startswith("$") and word.replace("$", "") in crypto_data.CRYPTO_TICKERS:
            word = word.replace("$", "")
            self.add_crypto_ticker(word, arText)
            return
        if word.startswith("NYSE:") or word.startswith("NASDAQ:") or word.startswith("$"):
            word = word.replace("NYSE:", "").replace("NASDAQ:", "").replace("$", "")
            if word.isdigit():
                return
            if word.isupper() and word in stock_data.DOWNLOADED_STOCK_TICKERS or word in stock_data.STOCK_TICKERS:
                self.add_stock_ticker(word, arText)
                return
        if word in stock_data.DOWNLOADED_STOCK_TICKERS or word in stock_data.STOCK_TICKERS:
            if word not in sozin_config.blacklist and word not in crypto_data.CRYPTO_TICKERS:
                if word.isupper() and not word.isdigit():
                    self.add_stock_ticker(word, arText)
                    return
        # -> 4. if word is in Special Tickers, crypto.
        if word in crypto_data.SPECIAL_TICKERS.keys():
            word = crypto_data.SPECIAL_TICKERS[word]
            self.add_crypto_ticker(word, arText)
            return
        # -> 5. if word is all lower and in blacklist, not a ticker.
        if len(word) > 1 and word[0].isupper() and word[1].islower():
            return
        if word.islower() or (word, w) in sozin_config.blacklist:
            return
        # -> 6. Check is the capitalized version is in tickers (shouldn't make it this far)
        if w in crypto_data.CRYPTO_TICKERS or w in stock_data.STOCK_TICKERS:
            word = w
        # -> 7. is it a crypto ticker? or stock ticker?
        if word in crypto_data.CRYPTO_TICKERS:
            # is Crypto
            self.add_crypto_ticker(word, arText)

    def add_crypto_ticker(self, word, arText):
        # counting tickers
        if word in self.cryto_tickers:
            self.cryto_tickers[word] += 1
            self.comments[word].append(arText)
            self.count += 1
        else:
            self.cryto_tickers[word] = 1
            self.comments[word] = [arText]
            self.count += 1

    def add_stock_ticker(self, word, arText):
        # counting tickers
        if word in self.stock_tickers:
            self.stock_tickers[word] += 1
            self.comments[word].append(arText)
            self.count += 1
        else:
            self.stock_tickers[word] = 1
            self.comments[word] = [arText]
            self.count += 1


class SozinAnalyzer:
    crypto_tickers: dict = None
    stock_tickers: dict = None
    comments = {}
    # Crypto
    c_tickers, c_scores, c_s, c_symbols, c_scoreVader = {}, {}, {}, {}, {}
    c_top_picks, c_times, c_top = [], [], []
    c_count = 0
    # Stocks
    s_tickers, s_scores, s_s, s_symbols, s_scoreVader = {}, {}, {}, {}, {}
    s_top_picks, s_times, s_top = [], [], []
    s_count = 0

    def __init__(self, cryto_tickers, stock_tickers, comments):
        self.clear_all()
        self.crypto_tickers = cryto_tickers
        self.stock_tickers = stock_tickers
        self.comments = comments
        if self.crypto_tickers is not None:
            self.analyze_crypto()
        if self.stock_tickers is not None:
            self.analyze_stocks()
        print("\nDONE\n")

    def get_results(self):
        results = SozinResults(self.crypto_tickers, self.c_scores, self.c_s, self.c_symbols, self.c_top,
                               self.stock_tickers, self.s_scores, self.s_s, self.s_symbols, self.s_top)
        return results.json

    def clear_all(self):
        self.crypto_tickers = {}    # -> All CRYPTO Tickers for the END
        self.stock_tickers = {}     # -> All STOCK Tickers for the END
        self.comments = {}          # -> All text analyzed and their senti scores
        # Crypto
        self.c_tickers = {}         # ->
        self.c_scores = {}          # -> HashMap<String<String,String>> Top 5 cryptos and their senti scores
        self.c_s = {}               # -> HashMap<String<String<String,Int>>> Crypto and a list of each comment and the comments senti score
        self.c_symbols = {}         # ->
        self.c_scoreVader = {}      # ->
        self.c_top_picks = []       # ->
        self.c_times = []           # ->
        self.c_top = []             # -> List<String,String/Int> of top tickers and their count
        self.c_count = 0            # ->
        # Stocks
        self.s_tickers = {}         # ->
        self.s_scores = {}          # ->
        self.s_s = {}               # ->
        self.s_symbols = {}         # ->
        self.s_scoreVader = {}      # ->
        self.s_top_picks = []       # ->
        self.s_times = []           # ->
        self.s_top = []             # ->
        self.s_count = 0            # ->

    def analyze_crypto(self):
        symbols = dict(sorted(self.crypto_tickers.items(), key=lambda item: item[1], reverse=True))
        top_picks = list(symbols.keys())[0:sozin_config.c_picks]
        print("\n Crypto Tickers ")
        for i in top_picks:
            print(f"{i}: {symbols[i]}")
            self.c_times.append(symbols[i])
            self.c_top.append(f"{i}: {symbols[i]}")
        vader = SentimentIntensityAnalyzer()
        # adding custom words from stock_data.py
        vader.lexicon.update(sozin_config.WEIGHTED_WORDS)
        picks_sentiment = list(symbols.keys())[0:sozin_config.c_picks_ayz]
        for symbol in picks_sentiment:
            stock_comments = self.comments[symbol]
            for cmnt in stock_comments:
                self.c_scoreVader = vader.polarity_scores(cmnt)
                if symbol in self.c_s:
                    self.c_s[symbol][cmnt] = self.c_scoreVader
                else:
                    self.c_s[symbol] = {cmnt: self.c_scoreVader}
                if symbol in self.c_scores:
                    for key, _ in self.c_scoreVader.items():
                        sV = self.c_scoreVader[key]
                        self.c_scores[symbol][key] += float(sV)
                else:
                    self.c_scores[symbol] = self.c_scoreVader
            # calculating avg.
            for key in self.c_scoreVader:
                self.c_scores[symbol][key] = self.c_scores[symbol][key] / symbols[symbol]
                self.c_scores[symbol][key] = "{pol:.3f}".format(pol=self.c_scores[symbol][key])
        # self.print_analysis(isCrypto=True)

    def analyze_stocks(self):
        symbols = dict(sorted(self.stock_tickers.items(), key=lambda item: item[1], reverse=True))
        top_picks = list(symbols.keys())[0:sozin_config.s_picks]
        print("\n Stock Tickers ")
        for i in top_picks:
            print(f"{i}: {symbols[i]}")
            self.s_times.append(symbols[i])
            self.s_top.append(f"{i}: {symbols[i]}")
        vader = SentimentIntensityAnalyzer()
        # adding custom words from stock_data.py
        vader.lexicon.update(sozin_config.WEIGHTED_WORDS)
        picks_sentiment = list(symbols.keys())[0:sozin_config.s_picks_ayz]
        for symbol in picks_sentiment:
            stock_comments = self.comments[symbol]
            for cmnt in stock_comments:
                self.s_scoreVader = vader.polarity_scores(cmnt)
                if symbol in self.s_s:
                    self.s_s[symbol][cmnt] = self.s_scoreVader
                else:
                    self.s_s[symbol] = {cmnt: self.s_scoreVader}
                if symbol in self.s_scores:
                    for key, _ in self.s_scoreVader.items():
                        self.s_scores[symbol][key] += self.s_scoreVader[key]
                else:
                    self.s_scores[symbol] = self.s_scoreVader
            # calculating avg.
            for key in self.s_scoreVader:
                self.s_scores[symbol][key] = self.s_scores[symbol][key] / symbols[symbol]
                self.s_scores[symbol][key] = "{pol:.3f}".format(pol=self.s_scores[symbol][key])
        # self.print_analysis(isCrypto=False)

    def print_analysis(self, isCrypto=True, isStock=True):
        if isCrypto:
            if len(self.c_scores) == 0:
                print("\nNo Crypto Scores\n")
            else:
                print(f"\nCRYPTO: Sentiment analysis of top {sozin_config.c_picks_ayz} picks:")
                # print("Scores:", scores)
                df = pd.DataFrame(self.c_scores)
                df.index = ['Bearish', 'Neutral', 'Bullish', 'Total/Compound']
                df = df.T
                print(df, "\n")
        if isStock:
            if len(self.s_scores) == 0:
                print("\nNo Stock Scores\n")
            else:
                print(f"\nSTOCKS: Sentiment analysis of top {sozin_config.s_picks_ayz} picks:")
                # print("Scores:", scores)
                df = pd.DataFrame(self.s_scores)
                df.index = ['Bearish', 'Neutral', 'Bullish', 'Total/Compound']
                df = df.T
                print(df, "\n")


class SozinResults:
    crypto_tickers: dict = None
    stock_tickers: dict = None
    # Crypto
    c_scores, c_s, c_symbols = {}, {}, {}
    c_top = []
    # Stocks
    s_scores, s_s, s_symbols = {}, {}, {}
    s_top = []
    json = {}

    def __init__(self, c_tickers, c_scores, c_s, c_symbols, c_top,
                 s_tickers, s_scores, s_s, s_symbols, s_top):
        self.crypto_tickers = c_tickers  # -> All CRYPTO Tickers for the END
        self.stock_tickers = s_tickers  # -> All STOCK Tickers for the END
        # Crypto
        self.c_scores = c_scores  # -> HashMap<String<String,String>> Top 5 cryptos and their senti scores
        self.c_s = c_s  # -> HashMap<String<String<String,Int>>> Crypto and a list of each comment and the comments senti score
        self.c_symbols = c_symbols  # ->
        self.c_top = c_top  # -> List<String,String/Int> of top tickers and their count
        # Stocks
        self.s_scores = s_scores  # ->
        self.s_s = s_s  # ->
        self.s_symbols = s_symbols  # ->
        self.s_top = s_top  # ->
        self.build()

    def build(self):
        self.json = {
            "crypto_tickers": self.crypto_tickers,
            "stock_tickers": self.stock_tickers,
            "c_scores": self.c_scores,
            "c_s": self.c_s,
            "c_symbols": self.c_symbols,
            "c_top": self.c_top,
            "s_scores": self.s_scores,
            "s_s": self.s_s,
            "s_symbols": self.s_symbols,
            "s_top": self.s_top
        }
