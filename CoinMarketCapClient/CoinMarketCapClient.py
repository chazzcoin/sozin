from requests import Session
from requests.exceptions import ConnectionError, Timeout, TooManyRedirects
import json


with open('data.json') as out:
    saved_json = json.load(out)
saved_data = saved_json.get('data')


url = 'https://pro-api.coinmarketcap.com/v1/cryptocurrency/listings/latest'
parameters = {
  'start':'1',
  'limit':'5000',
  'convert':'USD'
}
headers = {
  'Accepts': 'application/json',
  'X-CMC_PRO_API_KEY': 'f369741c-4e59-4089-9481-87d714ec1200',
}

session = Session()
session.headers.update(headers)

try:
    # -> Get Data from CoinMarketCap.
    response = session.get(url, params=parameters)
    data = json.loads(response.text)

    # -> Pull out the cryptos and their info
    items = data.get("data")

    # -> Validate Old and New are not the same.
    print("Old Data:", len(saved_data))
    print("New Data:", len(items))
    if len(items) == len(saved_data):
        print("Nothing New")
        exit()

    # -> Save New data.json info from CoinMarketCap.
    with open('data.json', 'w') as f:
        json.dump(data, f, sort_keys=True, indent=4)
        
    # -> Loop new items, look for the odd ones out.
    for item in items:
        i = item.get("symbol")
        print("Checking for:", i)
        if i in saved_data:
            continue
        else:
            print("Found New Project")
            print("\nsymbol:", item.get("symbol"))
            print("name:", item.get("name"))
            print("price:", item.get("quote").get("USD").get("price"))


except (ConnectionError, Timeout, TooManyRedirects) as e:
  print(e)