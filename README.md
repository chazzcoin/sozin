# Sozin Analyzer
This program goes through reddit, twitter and rss feeds to find the most mentioned 
tickers (stocks/cryptos) and topics (categories/key-terms) and uses Vader SentimentIntensityAnalyzer to calculate the ticker compound value.  

## Sources:
*See "sozin_utils/sozin_sources.py" for official source monitoring.
<pre>
1. Reddit
    a. Subreddit
        - Subscriber Avg/Movement
    b. Posts
        - Granted Users
        - Title Match
        - Upvote Threshold
        - Amount of Comments
    c. Comments
        - Granted Users
        - Text Match
        - Upvote Threshold
        - Amount of Child Comments
2. Twitter
    a. By User
3. RSS Feed Articles
    a. Stock Based Feeds
    b. Crypto Based Feeds
</pre>

## Text Analyzing For:
<pre>
1. Stock Tickers
2. Crypto Tickers
3. List of Categories(ByKeyTerms)
</pre>

## Analyzers to Build:
<pre>
1. Hype Meter
    [ Track Sentiment of Specific Ticker/Topic ]
    - Topic / Key-Term
    - Stock
    - Crypto
2. Stock Company Analyzer
    [ Match Ticker to Company ]
   - Parent Company
   - Price History
   - Volume History
   - Market Cap History
   - Earnings History
</pre>

## [REDDIT] Program Parameters:
* One for "Stocks" && One for "Crypto"
<pre>
subs = []           sub-reddit to search
post_flairs = {}    posts flairs to search || None flair is automatically considered
goodAuth = {}       authors whom comments are allowed more than once
uniqueCmt = True    allow one comment per author per symbol
ignoreAuthP = {}    authors to ignore for posts
ignoreAuthC = {}    authors to ignore for comment 
upvoteRatio = float upvote ratio for post to be considered, 0.70 = 70%
ups = int           define # of upvotes, post is considered if upvotes exceed this #
limit = int         define the limit, comments 'replace more' limit
upvotes = int       define # of upvotes, comment is considered if upvotes exceed this #
picks = int         define # of picks here, prints as "Top ## picks are:"
picks_ayz = int     define # of picks for sentiment analysis
comment_upvote_max = int  # Don't allow comments above threshold
comment_upvote_min = int  # Don't allow comments below threshold
</pre>

## Sample Output:
Reddit, Twitter and RSS Feed Articles analyzed...
* Sample Results from Reddit Only

It took 216.65 seconds to analyze 5862 comments in 80 posts in 4 subreddits.

 Crypto Tickers:\
BTC: 640\
ONE: 356\
DOGE: 264\
ETH: 213\
ADA: 167\
XRP: 94\
XLM: 61\
AAVE: 35\
LINK: 33\
DOT: 33

CRYPTO: Sentiment analysis of top 5 picks:\
&nbsp; &nbsp; &nbsp; &nbsp;    Bearish | Neutral | Bullish | Total/Compound\
BTC    &nbsp; 0.059 &nbsp; | &nbsp; 0.792 &nbsp; | &nbsp; 0.149 &nbsp; | &nbsp; 0.286\
ONE    &nbsp; 0.093 &nbsp; | &nbsp; 0.764 &nbsp; | &nbsp; 0.142 &nbsp; | &nbsp; 0.231\
DOGE   &nbsp; 0.092 &nbsp; | &nbsp; 0.763 &nbsp; | &nbsp; 0.145 &nbsp; | &nbsp; 0.236\
ETH    &nbsp; 0.051 &nbsp; | &nbsp; 0.816 &nbsp; | &nbsp; 0.133 &nbsp; | &nbsp; 0.296\
ADA    &nbsp; 0.052 &nbsp; | &nbsp; 0.780 &nbsp; | &nbsp; 0.169 &nbsp; | &nbsp;0.336 

 Stock Tickers:\
GME: 435\
AMC: 146\
BB: 55\
CRSR: 41\
AMD: 41\
TSLA: 40\
PLTR: 37\
AAPL: 28\
RIOT: 16\
MARA: 16

STOCKS: Sentiment analysis of top 5 picks:\
&nbsp; &nbsp; &nbsp; &nbsp;    Bearish | Neutral | Bullish | Total/Compound\
GME    &nbsp; 0.104 &nbsp; | &nbsp; 0.753 &nbsp; | &nbsp; 0.143 &nbsp; | &nbsp; 0.178\
AMC    &nbsp; 0.080 &nbsp; | &nbsp; 0.762 &nbsp; | &nbsp; 0.158 &nbsp; | &nbsp; 0.194\
BB   &nbsp; 0.049 &nbsp; | &nbsp; 0.734 &nbsp; | &nbsp; 0.217 &nbsp; | &nbsp; 0.336\
CRSR    &nbsp; 0.086 &nbsp; | &nbsp; 0.757 &nbsp; | &nbsp; 0.157 &nbsp; | &nbsp; 0.076\
AMD    &nbsp; 0.055 &nbsp; | &nbsp; 0.749 &nbsp; | &nbsp; 0.196 &nbsp; | &nbsp;0.548