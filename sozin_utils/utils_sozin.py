import datetime
import time
import os
from dateutil import parser

path = "/home/raava/bin/rsvaatu/SozinMaster/"
LINE = "\n------------------------------------------------------------------------------\n"
POST = LINE + " -- POST -- \n"
COMMENT = " --> "
n = "\n"
s = " "
co = ", "
p = "."
c = ":"


def get_date_time_for_db(t=None):
    if t is None:
        t = datetime.datetime.now()
    date = str(t.strftime("%A")) + co \
           + str(t.strftime("%B")) + s + str(t.strftime("%d")) + co + str(t.strftime("%Y")) + co \
           + str(t.strftime("%I")) + c + str(t.strftime("%M")) + c + str(t.strftime("%S")) + s + str(t.strftime("%p"))
    return date


def get_date_for_db(t=None):
    if t is None:
        t = datetime.datetime.now()
    date = str(t.strftime("%A")) + p + str(t.strftime("%B")) + p + str(t.strftime("%d")) + p + str(t.strftime("%Y"))
    return date


def get_timestamp():
    return str(time.time())


def get_now_date():
    return datetime.datetime.now().date()


def build_date(day, month, year):
    return datetime.datetime(year, month, day).date()


def build_date_for_db(day, month, year):
    return get_date_for_db(datetime.datetime(year, month, day))


def add_months(str_date, months=1):
    date = parser.parse(str_date)
    d = datetime
    month = date.month + months
    if month > 36:
        return None
    year = date.year
    if month > 24:
        year = date.year + 2
        month = month - 24
    else:
        if 12 < month < 24:
            year = date.year + 1
            month = month - 12
    new_date = d.datetime(year, month, date.day)
    return get_date_for_db(new_date)


def format_for_report(count, author, comment):
    final = n + str(count) + ". Author: " + str(author) + n + " -- " + str(comment) + n
    return final


def create_directory():
    os.mkdir(path + get_date_for_db())


def add_post(name, count, post_title, post_url):
    if not os.path.isdir(path + get_date_for_db()):
        create_directory()
    p = path + get_date_for_db() + "/"
    with open(p + name + ".txt", "a") as myfile:
        myfile.write(POST + str(count) + "." + s + post_title + n + post_url + n)


def add_comment(name, count, author, comment):
    p = path + get_date_for_db() + "/"
    with open(p + name + ".txt", "a") as myfile:
        myfile.write(format_for_report(count, author, comment))


def add_granted_user_comment(author, comment):
    p = path + get_date_for_db() + "/"
    with open(p + "user-comments" + ".txt", "a") as myfile:
        myfile.write(format_for_report(0, author, comment))
