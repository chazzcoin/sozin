
# -------------------------------------------------> TICKERS <------------------------------------------------------- #
# Match Allowed Tickers
CRYPTO_TICKERS = { 'BTC', 'XRP', 'XLM', 'ETH', 'VTHO', 'ALGO', 'ADA', 'DOT', 'LINK', 'BNB', 'BAT', 'MANA', 'REN', 'GRT',
                   'UNI', 'DOGE', 'XDG', 'EOS', 'VET', 'FLR', 'BAT', 'MIOTA', 'IOTA', 'DASH', 'ZRX', 'NANO', 'QTUM',
                   'SOLO', 'MKR', 'XEM', 'TRX', 'XMR', 'XTZ', 'NEO', 'DASH', 'NU', 'SUSHI', 'AAVE', 'UMA', 'BCH',
                   'WBTC', 'BSV', 'ATOM', 'THETA', 'EGLD', 'SOL', 'CRO', 'FIL', 'BTT', 'MATIC', 'LTC', 'HBAR', 'RVN',
                   'ONE', 'ENJ', 'BAND', 'OXT', 'HNT', 'KNC', 'ZEC', 'ZEN', 'WAVES', 'STORJ', 'REP', 'PAXG', 'ZIL'}
# Match Special Tickers for common crypto talk
SPECIAL_TICKERS = { 'Btc': 'BTC', 'Eth': 'ETH', 'Xrp': 'XRP', 'Xlm': 'XLM', 'eos': 'EOS', 'Doge': 'DOGE' }
# Matching Crypto based names to their Ticker
CRYPTO_NAMES = { 'ETHER': 'ETH', 'ETHEREUM': 'ETH', 'BITCOIN': 'BTC', 'RIPPLE': 'XRP', 'STELLAR': 'XLM',
                 'SUSHI': 'SUSHI', 'BITTORRENT': 'BTT', 'LITECOIN': 'LTC', 'CARDANO': 'ADA', 'ZCASH': 'ZEC',
                 'MONERO': 'XMR', 'DOGECOIN': 'DOGE'}