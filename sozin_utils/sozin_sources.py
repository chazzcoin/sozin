
# ------------------------------------------------> TWITTER <-------------------------------------------------------- #
# 1. Twitter Users to Monitor
twitter_users = ['mcuban', 'elonmusk', 'PeterLBrandt', 'CNBC', 'SJosephBurns', 'elerianm', 'IBDinvestors', 'jimcramer',
                 'bespokeinvest', 'wsbmod', 'galgitron']

# ------------------------------------------------> REDDIT <--------------------------------------------------------- #
# 1. Crypto Subreddit Communities to Monitor
crypto_reddit_communities = ['altstreetbets', 'CryptoCurrency', 'CryptoCurrencies', 'CryptoMarkets', 'cryptosignals',
                             'cryptospread', 'satoshistreetbets']

# 2. Stock Subreddit Communities to Monitor
stocks_reddit_communities = ['wallstreetbets', 'WallStreetbetsELITE', 'deepfuckingvalue', 'stocks', 'investing',
                             'stockmarket']

# 3. Posts flairs to search || None flair is automatically considered
post_flair = {'Daily Discussion', 'Weekend Discussion', 'Ticker Discussion', 'Discussion',
               'General News', 'Fundamentals', 'News', 'Gain', 'OFFICIAL', 'META', 'TRADING',
               'EDUCATIONAL', 'FOCUSED-DISCUSSION', 'Technical Analysis'}

# 4. Granted Users to Monitor
users_to_grant = { 'DeepFuckingValue' }

# -----------------------------------------> RSS FEEDS / ARTICLES <-------------------------------------------------- #
# 1. Stock Based RSS Feeds to Monitor
stocks_rss_url_list = ["http://feeds.marketwatch.com/marketwatch/topstories/", "https://seekingalpha.com/feed.xml",
                       "https://www.investing.com/rss/news_25.rss", "https://blog.wallstreetsurvivor.com/feed/",
                       "https://stockstotrade.com/blog/feed/", "https://www.cnbc.com/id/20409666/device/rss/rss.html?x=1",
                       "http://economictimes.indiatimes.com/markets/stocks/rssfeeds/2146842.cms",
                       "https://mebfaber.com/feed/", "http://welcome.philstockworld.com/feed/",
                       "https://www.reddit.com/r/stocks/.rss"]

# 2. Crypto Based RSS Feeds to Monitor
crypto_rss_url_list = ["https://cointelegraph.com/rss", "https://coindesk.com/feed", "https://news.bitcoin.com/feed",
                       "https://minergate.com/blog/feed/", "https://coinjournal.net/feed",
                       "https://cryptoinsider.com/feed", "http://www.newsbtc.com/feed",
                       "https://twitter.com/jaxx_io/feed", "https://bitcoinmagazine.com/feed",
                       "https://www.crypto-news.net/feed", "https://www.cryptoninjas.net/feed",
                       "https://ethereumworldnews.com/feed", "https://bravenewcoin.com/feed",
                       "http://www.financemagnates.com/feed", "http://www.cryptoquicknews.com/feed",
                       "http://cryptscout.com/feed", "http://www.coinnewsasia.com/feed"]
# Master List of RSS Feeds
rss_url_list = stocks_rss_url_list + crypto_rss_url_list

# -------------------------------------> CATEGORIES AND THEIR KEY-TERMS <-------------------------------------------- #
# 0. General Key-Terms
general_terms = []

# 1. Weed Key-Terms to Monitor
weed_terms = [ 'legalization', 'decriminalization', 'cannabis stock', 'cannabinoid-therapeutics', 'cannabinoid',
               'medicinal marijuana', 'cannabis investors', 'pot stock', 'cannabis legalization',
               'federal decriminalization', 'marijuana stocks', 'marijuana legalization bills', 'cannabis market',
               'cannabis', 'weed', 'marijuana']

# 2. Stock Key-Terms to Monitor
stock_terms = [ 'merge', 'merger', 'acquisition', 'stepping down', 'IPO', 'partnering' ]

# 3. Crypto Key-Terms to Monitor
crypto_terms = []

# Master List of Categories to Loop Through (Needs to be Last)
topics = { 'General': general_terms, 'Stocks': stock_terms, 'Crypto': crypto_terms, 'Weed': weed_terms }
