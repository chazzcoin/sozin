import pandas as pd
import json
from pandas.io.json import json_normalize


def save_csv_file(file_name, json_input):
    df = pd.DataFrame.from_dict(json_input['data'][0]['symbol'])
    df.columns = ['var1']
    df.to_csv(file_name)