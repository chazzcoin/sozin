from yahoo_fin import stock_info as si


# Get Current Crypto Price for Ticker
def get_crypto_price(ticker):
    cryptos = si.get_top_crypto().iterrows()
    for c in cryptos:
        c = c[1]
        if ticker.upper() in c['Name']:
            print(ticker + ":", c['Price (Intraday)'])
            return c