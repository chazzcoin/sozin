import pandas
from yahoo_fin import stock_info as si

from rsvaatu.SozinMaster.sozin_utils import utils_sozin

stock_file_path = '/home/sozin/bin/rsvaatu/SozinMaster/sozin_utils/stocks.csv'


# Get Average Stock Volume for TimeFrame
def get_average_stock_volume_for_timeframe(ticker, start_date=None, end_date=None):
    if start_date is None:
        t = utils_sozin.get_now_date()
        start_date = utils_sozin.build_date(t.day, t.month, t.year - 1)
    if end_date is None:
        end_date = utils_sozin.get_now_date()
    head = si.get_data(ticker.lower(), start_date, end_date)
    total = 0
    volume = head['volume']
    for vol in volume:
        total += int(vol)
    return total / len(volume)


# Get Current Stock Price for Ticker
def get_stock_price(ticker):
    # or any other ticker
    return si.get_live_price(ticker)


# Load Stock Ticker List from CSV File
def read_stock_csv():
    df = pandas.read_csv(stock_file_path)
    return df


# Parse Stock Ticker List from CSV File
def get_stocks_from_csv():
    df = read_stock_csv()
    list_of_tickers = []
    for ticker in df['Symbol']:
        list_of_tickers.append(ticker)
    return list_of_tickers
