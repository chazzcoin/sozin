import praw
from datetime import datetime
from rsvaatu.SozinMaster.sozin_utils import sozin_config, sozin_sources
from rsvaatu.fileutils import fileutils


class RedditClient:
    reddit: praw
    comments = []

    user_agent = sozin_config.reddit_user_agent
    client_id = sozin_config.reddit_client_id
    client_secret = sozin_config.reddit_client_secret
    username = sozin_config.reddit_username
    password = sozin_config.reddit_password

    def __init__(self, isCrypto=True, isStocks=True):
        self.reddit = praw.Reddit(user_agent=self.user_agent,
                                  client_id=self.client_id,
                                  client_secret=self.client_secret,
                                  username=self.username,
                                  password=self.password)
        # Do it once per flag
        if isCrypto:
            self.reddit_analysis(isCrypto=True)
        if isStocks:
            self.reddit_analysis(isCrypto=False)

    @staticmethod
    def init_new_client():
        return praw.Reddit(user_agent=sozin_config.reddit_user_agent,
                                  client_id=sozin_config.reddit_client_id,
                                  client_secret=sozin_config.reddit_client_secret,
                                  username=sozin_config.reddit_username,
                                  password=sozin_config.reddit_password)

    @staticmethod
    def grab_user_comments(username):
        reddit = RedditClient.init_new_client()
        comments = reddit.redditor(username).comments.new(limit=None)
        file_name = f"{username}_comments_bham"
        file_path = "/home/sozin/bin/"
        fileutils.create_open_file(file_name, file_path)
        com = []
        for c in comments:
            temp = {}
            temp["date"] = datetime.utcfromtimestamp(c.created_utc).strftime('%h/%d/%Y %I:%M:%S %p')
            temp["Subreddit"] = f"r/{str(c.subreddit)}"
            temp["body"] = c.body
            com.append(temp)
            print("\n--------NEW COMMENT----------\n")
            print("Comment Date:", datetime.utcfromtimestamp(c.created_utc).strftime('%h/%d/%Y %I:%M:%S %p'))
            print(f"Subreddit: r/{str(c.subreddit)}")
            print(c.body)

        fileutils.build_file_with_data(file_name, file_path, com)

    @staticmethod
    def grab_user_posts(username):
        reddit = RedditClient.init_new_client()
        comments = reddit.redditor(username).submissions.new(limit=None)
        file_name = f"{username}_posts"
        file_path = "/home/sozin/bin/"
        fileutils.create_open_file(file_name, file_path)
        com = []
        for c in comments:
            temp = {}
            temp["title"] = c.title
            temp['date'] = datetime.utcfromtimestamp(c.created_utc).strftime('%h/%d/%Y %I:%M:%S %p')
            temp['subreddit'] = f"r/{str(c.subreddit)}"
            temp['url'] = c.url
            com.append(temp)
            print("\n--------NEW POST----------\n")
            print("Post Title:", c.title)
            print("Post Date:", datetime.utcfromtimestamp(c.created_utc).strftime('%h/%d/%Y %I:%M:%S %p'))
            print(f"Subreddit: r/{c.subreddit}")
            print("Post URL:", c.url)
        fileutils.build_file_with_data(file_name, file_path, com)

    def reddit_analysis(self, isCrypto=True):
        # Set Global Thresholds
        if isCrypto:  # Crypto Setup
            communities = sozin_sources.crypto_reddit_communities
            ignoreAuthP = sozin_config.c_ignoreAuthP
            ignoreAuthC = sozin_config.c_ignoreAuthC
            ups = sozin_config.c_ups
            upvotes = sozin_config.c_upvotes
            upvoteRatio = sozin_config.c_upvoteRatio
            limit = sozin_config.c_limit
            flairs = sozin_sources.post_flair
        else:  # Stocks Setup
            communities = sozin_sources.stocks_reddit_communities
            ignoreAuthP = sozin_config.s_ignoreAuthP
            ignoreAuthC = sozin_config.s_ignoreAuthC
            ups = sozin_config.s_ups
            upvotes = sozin_config.s_upvotes
            upvoteRatio = sozin_config.s_upvoteRatio
            limit = sozin_config.s_limit
            flairs = sozin_sources.post_flair

        # -> REDDIT SUB / POST / COMMENT SORTING <- #
        print("\n -> Running Reddit Analysis <- \n")
        # -> 1. LOOP SUBREDDITS
        for subreddit in communities:
            sub = self.reddit.subreddit(subreddit)
            # Print Subreddit Title
            print(subreddit.title(), "\n")
            # sorting posts by hot
            hot_python = sub.hot()
            # -> 2. LOOP POSTS
            for post in hot_python:
                flair = post.link_flair_text
                author = str(post.author)
                # checking post upvote ratio # of upvotes, post flair, and author
                if post.upvote_ratio >= upvoteRatio and post.ups > ups and (
                        flair in flairs or flair is None) and author not in ignoreAuthP:
                    post.comment_sort = 'new'
                    comments = post.comments
                    post.comments.replace_more(limit=limit)
                    # -> 3. LOOP COMMENTS
                    for comment in comments:
                        # try except for deleted account?
                        auth = str(comment.author)
                        # -> COMMENTS : checking comment upvotes and author
                        if comment.score > upvotes and auth not in ignoreAuthC:
                            self.comments.append(comment.body)


RedditClient.grab_user_comments('everwinter81')