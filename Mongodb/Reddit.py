import json

class Comment:
    author = ""
    body = ""
    score = ""
    date = ""
    json = {}

    def __init__(self, author, body, score, date):
        self.author = author
        self.body = body
        self.score = score
        self.date = date
        self.build()

    def build(self):
        self.json = {
            "author": self.author,
            "body": self.body,
            "score": self.score,
            "date": self.date
        }


class Post:
    title = ""
    author = ""
    url = ""
    score = ""
    comments = []
    json = {}

    def __init__(self, title, author, url, score, date):
        self.author = author
        self.title = title
        self.score = score
        self.url = url
        self.date = date
        self.build()

    def build(self):
        self.json = {
            "author": self.author,
            "title": self.title,
            "url": self.url,
            "score": self.score,
            "date": self.date,
            "comments": self.comments
        }

    def add_comment(self, comment: Comment):
        self.comments.append(comment.json)


# ['Bearish', 'Neutral', 'Bullish', 'Total/Compound']
class Ticker:
    ticker_name = ""
    bearish_count = ""
    neutral_count = ""
    bullish_count = ""
    total_compound = ""
    json = {}

    def build(self):
        self.json = {
            "ticker_name": self.ticker_name,
            "bearish_count": self.bearish_count,
            "neutral_count": self.neutral_count,
            "bullish_count": self.bullish_count,
            "total_compound": self.total_compound
        }


class Sentiment:
    tickers: [Ticker]
    top_tickers = {}
    json = {}

    def build(self):
        self.json = {
            "tickers": self.tickers,
            "top_tickers": self.top_tickers
        }