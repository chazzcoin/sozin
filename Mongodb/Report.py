from models import mReddit
import json as j


def parse(obj):
    return str(obj).encode("utf8").decode("utf8")


def build(posts: [mReddit.Post]):
    l = []
    for post in posts:
        js = post.get("json")
        print(js)
        l.append(js)
    return j.dumps(l)


class Report:
    """We will be adding new Report Objects from different sources"""
    posts: {}
    top_ten_tickers = []
    sentiment_scores = {}
    sentiment_analysis = ""
    json = {}  # Will need to build all sources into single json report or multi?

    def __init__(self, posts, top_ten: [], scores: {}, analysis):
        self.post = posts
        self.top_ten_tickers = top_ten
        self.sentiment_scores = scores
        self.sentiment_analysis = Report.parse_analysis(analysis)
        self.json = {
            "posts": posts,
            "top_ten_tickers": top_ten,
            "sentiment_scores": scores,
            "sentiment_analysis": analysis
        }

    @staticmethod
    def parse(obj):
        newReport = Report
        report = obj.get('report')
        newReport.posts = report.get('posts')
        newReport.top_ten_tickers = report.get('top_ten_tickers')
        newReport.sentiment_scores = report.get('sentiment_scores')
        newReport.sentiment_analysis = report.get('sentiment_analysis')
        return newReport

    @staticmethod
    def parse_analysis(analysis):
        return str(analysis) \
            .replace("\"", "\'") \
            .replace(", \'", ", \"") \
            .replace("{\'", "{\"") \
            .replace(": \'", ": \"") \
            .replace("\':", "\":") \
            .replace("\', \"", "\", \"") \
            .replace("\'}", "\"}") \
            .replace("\\", "") \
            .replace(":", "+") \
            .replace("\"+ {", "\": {") \
            .replace("\"+ ", "\": ") \
            .replace("\"+ \"", "\": \"")

