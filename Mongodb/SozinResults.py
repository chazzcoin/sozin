

class SozinResults:
    crypto_tickers: dict = None
    stock_tickers: dict = None
    # Crypto
    c_scores, c_s, c_symbols = {}, {}, {}
    c_top = []
    # Stocks
    s_scores, s_s, s_symbols = {}, {}, {}
    s_top = []
    json = {}

    def __init__(self, c_tickers, c_scores, c_s, c_symbols, c_top,
                 s_tickers, s_scores, s_s, s_symbols, s_top):
        self.crypto_tickers = c_tickers  # -> All CRYPTO Tickers for the END
        self.stock_tickers = s_tickers  # -> All STOCK Tickers for the END
        # Crypto
        self.c_scores = c_scores  # -> HashMap<String<String,String>> Top 5 cryptos and their senti scores
        self.c_s = c_s  # -> HashMap<String<String<String,Int>>> Crypto and a list of each comment and the comments senti score
        self.c_symbols = c_symbols  # ->
        self.c_top = c_top  # -> List<String,String/Int> of top tickers and their count
        # Stocks
        self.s_scores = s_scores  # ->
        self.s_s = s_s  # ->
        self.s_symbols = s_symbols  # ->
        self.s_top = s_top  # ->
        self.build()

    def build(self):
        self.json = {
            "crypto_tickers": self.crypto_tickers,
            "stock_tickers": self.stock_tickers,
            "c_scores": self.c_scores,
            "c_s": self.c_s,
            "c_symbols": self.c_symbols,
            "c_top": self.c_top,
            "s_scores": self.s_scores,
            "s_s": self.s_s,
            "s_symbols": self.s_symbols,
            "s_top": self.s_top
        }