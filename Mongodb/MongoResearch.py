from pymongo import MongoClient
from pymongo.database import Database
import utils.DateUtils
import utils.rsConfig
from models.mReport import Report
import json as j
import re


def parse(obj):
    return str(obj).encode("utf8").decode("utf8")


class MongoResearch:
    client: MongoClient
    db_research: Database
    c_news: Database

    def __init__(self, url=utils.rsConfig.sozin_mongo_db_uri):
        self.client = MongoClient(url)
        self.db_research = self.client.get_database("research")
        self.c_news = self.db_research.get_collection("news")

    def find_all_news_reports(self):
        for a in self.c_news.find():
            print(a)

    def add_update_report(self, news_type, new_report):
        self.c_news.insert_one({ news_type: utils.DateUtils.get_date_for_db_with_space(), "report": new_report })

    def get_report_for_date(self, news_type, date):
        # if not self.room_exists(room_name=room_name):
        #     return None
        temp = self.c_news.find( { news_type: date } ).next()
        report = Report.parse(temp)
        # print("Returning Room: ", report)
        return report

db = MongoResearch()
print(db.find_all_news_reports())
