from pymongo import MongoClient
from pymongo.database import Database
from SozinMaster.Mongodb import MongoConfig


class MongoDB:
    """
    -> dbVaatuMongo.rsMongoDB.MongoDB
    -> Helper Module for MongoDB
    """
    client: MongoClient
# -> Databases <- #
    db_admin: Database
    db_config: Database
    db_sozin: Database
# -> Collections <- #
    c_sozin: Database     # db_sozin

# -> Variables <- #
    v_hashed_password = ''
    password = ""

    def __init__(self, url=MongoConfig.mongo_db_uri):
        """MongoDB Instance Init
        :param url="default to SozinMaster (docker)"
        """
        self.client = MongoClient(url)
        self.init_databases()
        self.init_collections()

    def init_databases(self):
        """Init All Databases"""
        self.db_admin = self.client.get_database("admin")
        self.db_config = self.client.get_database("config")
        self.db_local = self.client.get_database("local")
        self.db_sozin = self.client.get_database("sozin")

    def init_collections(self):
        """Init All Collections"""
        self.c_sozin = self.db_sozin.get_collection("sozin")

    def close(self):
        """-> Closes out self.client instance.<-"""
        print("Closing MongoDB Instance")
        self.client.close()

# -> SHOW/FIND LIST OF <- #
    def find_databases(self):
        """FIND/PRINT ->Prints full list of databases in MongoDB.<-"""
        print(self.client.list_database_names())

    def find_sozin_collections(self):
        """FIND/PRINT ->Prints full list of collections in databases.<-"""
        print(self.db_sozin.list_collection_names())