""" -> SERVER INFO <- """
server_ip = "192.168.1.184"
server_name = "SozinMaster"
user_name = "sozin"
cell_number = "2052493671"

# -> MASTER PATH <- #
master_path = "/home/" + user_name + "/bin"

# -> MONGO DATABASE <- #
mongo_ip = server_ip
mongo_port = "27017"
mongo_db_uri = "Mongodb://" + mongo_ip + ":" + mongo_port

# -> SMS NOTIFICATIONS <- #
# TextBelt: https://textbelt.com/ #
sms_key = "3b11437549708345091de04ca87cba6129cdfb493NIMu5rAxH3fm9Ie8EEvYqrZw"
sms_url = 'https://textbelt.com/text'
sms_number = cell_number

# -> POWER MESSAGES <- #
message_power_loss = server_name + " has lost power and is on backup!"
message_power_critical = server_name + " power critical: "
message_server_loss = "We have lost contact with " + server_name + ". Services may be down."
